.text
.align 2
.global sum1
.type sum1, %function
.global asd1
.type asd1, %function

.global setPixColor
.type setPixColor, %function

.global getcurcol
.type getcurcol, %function

.global pixel
.type pixel, %function

.global circle
.type circle, %function

.global line
.type line, %function



/******************************************************************/

setPixColor:
ldr r0,[r0]
ldr r1,CurColourAddr
str r0,[r1]
bx lr


getcurcol:
ldr r0,CurColourAddr
bx lr
/******************************************************************/

pixel:

ldr r2,[r2]
stmfd sp!,{r0-r5,lr}

@noskaidro vai 0<=x<=width
cmp r0,#0
blt pixelEnd


bl FrameBufferGetWidth
ldr r1,[sp]@x vertiba
cmp r1,r0
bge pixelEnd
@///////////////


@noskaidro vai 0<=y<=height
ldr r0,[sp,#4]
cmp r0,#0
blt pixelEnd


bl FrameBufferGetHeight
ldr r1,[sp,#4] @x vertiba

cmp r1,r0
bge pixelEnd
@///////////////


@iekraso pixeli(x,y) jeb F[y][x]
bl FrameBufferGetWidth
mov r4,r0
bl FrameBufferGetAddress
mov r5,r0


ldr r1,[sp]
ldr r2,[sp,#4]
ldr r3,[sp,#8]

mul r0,r2,r4
add r0,r0,r1


str r3,[r5,r0,LSL #2]
@///////////////


pixelEnd:
ldmfd sp!,{r0-r5,lr}
bx lr

/*****************************************************************/

circle:

stmfd sp!,{r0-r10,lr}

bl FrameBufferGetWidth
mov r6,r0
bl FrameBufferGetHeight
mov r7,r0
bl FrameBufferGetAddress
mov r8,r0

ldr r9,CurColourAddr



/****/
mov r1,#0
ldr r3,[sp,#8]
mov r4,#-1
mul r0,r3,r4
mov r2,#1



circleLoopCheck:
cmp r0,#0
bgt circleLoopEnd

circleLoop:

@sakas while
    circleLoop2Check:
    cmp r2,#0
    bge circleLoop2End

    circleLoop2:
    add r1,r1,#1

    mov r3,r1,LSL #1
    add r3,r3,#1

    add r2,r2,r3
    b circleLoop2Check

    circleLoop2End:
@beidzas while

mov r3,r1,LSL #1
add r3,r3,#1
mov r4,r2, LSL #1
sub r4,r4,r3

cmp r4,#0
bgt circleSkip1
add r1,r1,#1
mov r3,r1,LSL #1
add r3,r3,#1
add r2,r2,r3

circleSkip1:


@sakas krasosana

sub sp,sp,#4
str r2,[sp]
/**/

ldr r2,[sp,#4]@r2=xr
ldr r3,[sp,#8]@r3=y3

@x,y
mov r4,r0
add r4,r4,r2

mov r5,r1
add r5,r5,r3
bl circleKrasosana



@y,x
mov r4,r1
add r4,r4,r2

mov r5,r0
add r5,r5,r3
bl circleKrasosana




@x,-y
mov r4,r0
add r4,r4,r2

mov r5,#0
sub r5,r5,r1
add r5,r5,r3
bl circleKrasosana


@-y,x
mov r4,#0
sub r4,r4,r1
add r4,r4,r2

mov r5,r0
add r5,r5,r3
bl circleKrasosana



@-x,y
mov r4,#0
sub r4,r4,r0
add r4,r4,r2

mov r5,r1
add r5,r5,r3
bl circleKrasosana


@y,-x
mov r4,r1
add r4,r4,r2

mov r5,#0
sub r5,r5,r0
add r5,r5,r3
bl circleKrasosana

/*
@@
mov r3,r0
mov r10,r1
mov r1,r4
bl myPrint
mov r1,r5
bl myPrint
mov r0,r3
mov r1,r10
@@
*/



@-x,-y
mov r4,#0
sub r4,r4,r0
add r4,r4,r2

mov r5,#0
sub r5,r5,r1
add r5,r5,r3
bl circleKrasosana


@-y,-x
mov r4,#0
sub r4,r4,r1
add r4,r4,r2

mov r5,#0
sub r5,r5,r0
add r5,r5,r3
bl circleKrasosana




/**/
ldr r2,[sp]
add sp,sp,#4
@beidzas krasosana

mov r3,r0,LSL #1
add r3,r3,#1
add r2,r2,r3

add r0,r0,#1
b circleLoopCheck
circleLoopEnd:
/****/


ldmfd sp!,{r0-r10,lr}
bx lr





/********/
circleKrasosana:


@vai x ok
cmp r4,#0
blt circleKrasosanaEnd
cmp r4,r6
bge circleKrasosanaEnd

@vai y ok
cmp r5,#0
blt circleKrasosanaEnd
cmp r5,r7
bge circleKrasosanaEnd



@izrekina indeksu
mul r10,r5,r6
add r10,r10,r4


@nokraso
str r9,[r8,r10,LSL #2]


circleKrasosanaEnd:
bx lr





/*****************************************************************/

line:
stmfd sp!,{r0-r10,lr}
sub sp,sp,#16

bl FrameBufferGetWidth
str r0,[sp]
bl FrameBufferGetHeight
str r0,[sp,#4]
bl FrameBufferGetAddress
str r0,[sp,#8]
ldr r0,CurColourAddr
str r0,[sp,#12]


@izrekina x delta
ldr r0,[sp,#16]
ldr r1,[sp,#24]
sub r4,r0,r1
mov r3,r4
cmp r3,#0
it lt
neglt r3,r3

@izrekina y delta
ldr r0,[sp,#20]
ldr r1,[sp,#28]
sub r5,r0,r1
mov r6,r5
cmp r6,#0
it lt
neglt r6,r6

cmp r3,r6
blt lineY


@rekina pa x sakums

@izrekina k
    mov r0,r4
    bl __aeabi_i2d
    mov r4,r0
    mov r0,r5
    bl __aeabi_i2d
    mov r5,r0

    mov r0,r4
    mov r1,r5
    bl __aeabi_ddiv
    mov r7,r0

@izrekina b
    ldr r0,[sp,#16]
    bl __aeabi_i2d
    mov r1,r7
    bl __aeabi_dmul
    mov r4,r0

    ldr r0,[sp,#20]
    bl __aeabi_i2d
    mov r1,r4
    bl __aeabi_dsub
    mov r1,#0
    ldr r1, .LvienaPuse
    bl __aeabi_dadd
    mov r8,r0

@@@@
mov r0,#2
bl __aeabi_i2d
mov r4,r0

mov r0,#1
bl __aeabi_i2d
mov r1,r4
bl __aeabi_ddiv
mov r1,#132
bl myPrint
b lineY
@@@@

@ja x1>x2:swapo
ldr r0,[sp,#16]
ldr r1,[sp,#24]
cmp r0,r1
ble lineXNoSwap
mov r3,r0
mov r0,r1
mov r1,r3
lineXNoSwap:



mov r9,r0
mov r6,r1

lineXCheck:
cmp r9,r6
bgt lineXLoopEnd

lineXLoop:
mov r0,r9
bl __aeabi_i2d
mov r1,r7
bl __aeabi_dmul
mov r1,r8
bl __aeabi_dadd
bl __aeabi_d2iz
mov r10,r0

@kraso x,y sak
@kraso x,y beidz

add r9,r9,#1
b lineXCheck

lineXLoopEnd:
@rekina pa x beigas


lineY:

add sp,sp,#16
ldmfd sp!,{r0-r10,lr}
bx lr


.LvienaPuse:
    .word 1071644672






/*****************************************************************/

sum1:

add r0,r0,r1
mov r3,lr
mov r1,r0
bl myPrint
mov lr,r3
bx lr




myPrint:
stmfd sp!,{r0-r4,lr}
ldr r0, f_a
bl printf

ldmfd sp!,{r0-r4,lr}
bx lr



CurColourAddr: .word CurColour
            .comm CurColour,4,2


f_a: .word format
.data
format: .asciz "qw:%f\n"


