CC=arm-linux-gnueabi-gcc
AS=arm-linux-gnueabi-as



ASFLAGS = -mcpu=xscale  -alh=$*.lis -L
CFLAGS = -O0  -Wall
LDFLAGS = −g





All: agra_main.o framebuffer.o agra.o
	$(CC) agra_main.o framebuffer.o agra.o    -o agra



agra_main.o: agra_main.c    agra.h
	$(CC) agra_main.c   -g  $(CFLAGS) -c  -o agra_main.o



framebuffer.o: framebuffer.c    agra.h
	$(CC) framebuffer.c   -g  $(CFLAGS) -c  -o framebuffer.o



agra.o: agra.s agra.h
	$(AS) agra.s $(ASFLAGS) -g -o agra.o


test: All
	qemu-arm  -L /usr/arm-linux-gnueabi   agra

