.text
.align 2
.global sum1
.type sum1, %function
.global asd1
.type asd1, %function

.global setPixColor
.type setPixColor, %function

.global getcurcol
.type getcurcol, %function

.global pixel
.type pixel, %function

.global circle
.type circle, %function

.global line
.type line, %function


.global triangleFill
.type triangleFill, %function




/******************************************************************/

setPixColor:
ldr r0,[r0]
ldr r1,CurColourAddr
str r0,[r1]
bx lr


getcurcol:
ldr r0,CurColourAddr
bx lr
/******************************************************************/

pixel:

ldr r2,[r2]
stmfd sp!,{r0-r5,lr}

@noskaidro vai 0<=x<=width
cmp r0,#0
blt pixelEnd


bl FrameBufferGetWidth
ldr r1,[sp]@x vertiba
cmp r1,r0
bge pixelEnd
@///////////////


@noskaidro vai 0<=y<=height
ldr r0,[sp,#4]
cmp r0,#0
blt pixelEnd


bl FrameBufferGetHeight
ldr r1,[sp,#4] @x vertiba

cmp r1,r0
bge pixelEnd
@///////////////


@iekraso pixeli(x,y) jeb F[y][x]
bl FrameBufferGetWidth
mov r4,r0
bl FrameBufferGetAddress
mov r5,r0


ldr r1,[sp]
ldr r2,[sp,#4]
ldr r3,[sp,#8]

mul r0,r2,r4
add r0,r0,r1


str r3,[r5,r0,LSL #2]
@///////////////


pixelEnd:
ldmfd sp!,{r0-r5,lr}
bx lr

/*****************************************************************/

circle:

stmfd sp!,{r0-r10,lr}

bl FrameBufferGetWidth
mov r6,r0
bl FrameBufferGetHeight
mov r7,r0
bl FrameBufferGetAddress
mov r8,r0

ldr r9,CurColourAddr
ldr r9,[r9]



/****/
mov r1,#0
ldr r3,[sp,#8]
mov r4,#-1
mul r0,r3,r4
mov r2,#1



circleLoopCheck:
cmp r0,#0
bgt circleLoopEnd

circleLoop:

@sakas while
    circleLoop2Check:
    cmp r2,#0
    bge circleLoop2End

    circleLoop2:
    add r1,r1,#1

    mov r3,r1,LSL #1
    add r3,r3,#1

    add r2,r2,r3
    b circleLoop2Check

    circleLoop2End:
@beidzas while

mov r3,r1,LSL #1
add r3,r3,#1
mov r4,r2, LSL #1
sub r4,r4,r3

cmp r4,#0
bgt circleSkip1
add r1,r1,#1
mov r3,r1,LSL #1
add r3,r3,#1
add r2,r2,r3

circleSkip1:


@sakas krasosana

sub sp,sp,#4
str r2,[sp]
/**/

ldr r2,[sp,#4]@r2=xr
ldr r3,[sp,#8]@r3=y3

@x,y
mov r4,r0
add r4,r4,r2

mov r5,r1
add r5,r5,r3
bl circleKrasosana



@y,x
mov r4,r1
add r4,r4,r2

mov r5,r0
add r5,r5,r3
bl circleKrasosana




@x,-y
mov r4,r0
add r4,r4,r2

mov r5,#0
sub r5,r5,r1
add r5,r5,r3
bl circleKrasosana


@-y,x
mov r4,#0
sub r4,r4,r1
add r4,r4,r2

mov r5,r0
add r5,r5,r3
bl circleKrasosana



@-x,y
mov r4,#0
sub r4,r4,r0
add r4,r4,r2

mov r5,r1
add r5,r5,r3
bl circleKrasosana


@y,-x
mov r4,r1
add r4,r4,r2

mov r5,#0
sub r5,r5,r0
add r5,r5,r3
bl circleKrasosana

/*
@@
mov r3,r0
mov r10,r1
mov r1,r4
bl myPrint
mov r1,r5
bl myPrint
mov r0,r3
mov r1,r10
@@
*/



@-x,-y
mov r4,#0
sub r4,r4,r0
add r4,r4,r2

mov r5,#0
sub r5,r5,r1
add r5,r5,r3
bl circleKrasosana


@-y,-x
mov r4,#0
sub r4,r4,r1
add r4,r4,r2

mov r5,#0
sub r5,r5,r0
add r5,r5,r3
bl circleKrasosana




/**/
ldr r2,[sp]
add sp,sp,#4
@beidzas krasosana

mov r3,r0,LSL #1
add r3,r3,#1
add r2,r2,r3

add r0,r0,#1
b circleLoopCheck
circleLoopEnd:
/****/


ldmfd sp!,{r0-r10,lr}
bx lr





/********/
circleKrasosana:


@vai x ok
cmp r4,#0
blt circleKrasosanaEnd
cmp r4,r6
bge circleKrasosanaEnd

@vai y ok
cmp r5,#0
blt circleKrasosanaEnd
cmp r5,r7
bge circleKrasosanaEnd



@izrekina indeksu
mul r10,r5,r6
add r10,r10,r4


@nokraso
str r9,[r8,r10,LSL #2]


circleKrasosanaEnd:
bx lr





/*****************************************************************/

line:
stmfd sp!,{r0-r10,lr}


@izrekina deltas
ldr r0,[sp]
ldr r1,[sp,#8]
sub r0,r0,r1
cmp r0,#0
it lt
neglt r0,r0

ldr r1,[sp,#4]
ldr r2,[sp,#12]
sub r1,r1,r2
cmp r1,#0
it lt
neglt r1,r1

cmp r0,r1
blt lineY

@ja dx=0, dy=0
cmp r0,#0
bne lineX
ldr r7,[sp]
ldr r9,[sp,#4]
bl lineKraso
b lineEnd



lineX:
@Rekina pa X
ldmfd sp,{r0-r3}
sub r4,r1,r3
sub r5,r0,r2

mul r7,r3,r0
mov r6,r7
mul r7,r1,r2
sub r6,r6,r7



@ja x1>x2 swapo
ldr r7,[sp]
ldr r8,[sp,#8]
cmp r7,r8
ble lineSkip1
mov r2,r7
mov r7,r8
mov r8,r2
lineSkip1:




lineLoopCheck:
cmp r7,r8
bgt lineLoopEnd

lineLoop:

    mul r3,r4,r7
    add r3,r3,r6
    mov r0,r3
    mov r1,r5
    bl __aeabi_idiv
    mov r9,r0



    mul r0,r9,r5

    mul r3,r4,r7
    add r3,r3,r6

    sub r0,r0,r3
    add r1,r0,r5
    cmp r0,#0
    it lt
    neglt r0,r0



    cmp r1,#0
    it lt
    neglt r1,r1

    cmp r0,r1
    it pl
    addgt r9,r9,#1


    bl lineKraso

    add r7,r7,#1
    b lineLoopCheck
lineLoopEnd:
b lineEnd



lineY:
/** lineY sakums **/


@Rekina pa y

@ielade punktus, bet samaina x un y
ldr r0,[sp,#4]
ldr r1,[sp]

ldr r2,[sp,#12]
ldr r3,[sp,#8]
add sp,sp,#16
stmfd sp!,{r0-r3}



sub r4,r1,r3
sub r5,r0,r2

mul r7,r3,r0
mov r6,r7
mul r7,r1,r2
sub r6,r6,r7



@ja x1>x2 swapo
ldr r7,[sp]
ldr r8,[sp,#8]
cmp r7,r8
ble lineSkip2
mov r2,r7
mov r7,r8
mov r8,r2
lineSkip2:


lineYLoopCheck:
cmp r7,r8
bgt lineYLoopEnd

lineYLoop:

    mul r3,r4,r7
    add r3,r3,r6
    mov r0,r3
    mov r1,r5
    bl __aeabi_idiv
    mov r9,r0

    mul r0,r9,r5

    mul r3,r4,r7
    add r3,r3,r6

    sub r0,r0,r3
    add r1,r0,r5
    cmp r0,#0
    it lt
    neglt r0,r0

    cmp r1,#0
    it lt
    neglt r1,r1

    cmp r0,r1
    addgt r9,r9,#1


@samaina x,y
mov r10,r7
mov r7,r9
mov r9,r10

    bl lineKraso

@samaina x,y
mov r10,r7
mov r7,r9
mov r9,r10




    add r7,r7,#1
    b lineYLoopCheck
lineYLoopEnd:

/** lineY beigas **/




lineEnd:

ldmfd sp!,{r0-r10,lr}
bx lr


lineKraso:
sub sp,sp,#4
str lr,[sp]

@x ok
cmp r7,#0
blt endLineKraso

bl FrameBufferGetWidth
mov r10,r0
cmp r7,r0
bge endLineKraso


@y ok
cmp r9,#0
blt endLineKraso

bl FrameBufferGetHeight
cmp r9,r0
bge endLineKraso

@indekss
mov r0,r10
mul r10,r9,r0
add r10,r10,r7


bl FrameBufferGetAddress
ldr r1,CurColourAddr
ldr r1,[r1]

str r1,[r0,r10,LSL #2]

endLineKraso:
ldr lr,[sp]
add sp,sp,#4

bx lr




/*****************************************************************/

triangleFill:

ldr r4,[sp]
ldr r5,[sp,#4]

stmfd sp!,{r0-r10,lr}
sub sp,sp,#24

@izrekina v12
    sub r9,r2,r0
    sub r10,r3,r1
    str r9,[sp]
    str r10,[sp,#4]
@izrekina v13
    sub r9,r4,r0
    sub r10,r5,r1
    str r9,[sp,#8]
    str r10,[sp,#12]
@izrekina v23
    sub r9,r4,r2
    sub r10,r5,r3
    str r9,[sp,#16]
    str r10,[sp,#20]
/*********************/

@atrod xmin,xmax
    mov r9,r0
    mov r10,r0

    cmp r9,r2
    movgt r9,r2


    cmp r10,r2
    movlt r10,r2


    cmp r9,r4
    movgt r9,r4


    cmp r10,r4
    movlt r10,r4

    stmfd sp!,{r9,r10}



@atrod ymin,ymax
    mov r9,r1
    mov r10,r1

    cmp r9,r3
    movgt r9,r3


    cmp r10,r3
    movlt r10,r3

    cmp r9,r5
    movgt r9,r5

    cmp r10,r5
    movlt r10,r5

ldr r4,[sp] @xmin
ldr r5,[sp,#4] @xmax
add sp,sp,#8
mov r6,r9 @ymin
mov r7,r10 @ymax
/***********************/




triangleLoop1Check:
    cmp r4,r5
    bgt triangleLoop1End
triangleLoop1:
    mov r8,r6
    triangleLoop2Check:
        cmp r8,r7
        bgt triangleLoop2End
    triangleLoop2:
        @rekina par punktu sakums

        @cross1
            ldr r9,[sp,#24]
            sub r2,r4,r9
            ldr r9,[sp,#28]
            sub r3,r8,r9

            ldr r0,[sp]
            ldr r1,[sp,#4]

            mul r9,r2,r1
            mul r10,r3,r0
            sub r9,r9,r10

            cmp r9, #0
            blt triangleSkip
        @cross2
            ldr r0,[sp,#8]
            ldr r1,[sp,#12]

            mul r9,r0,r3
            mul r10,r1,r2
            sub r9,r9,r10

            cmp r9, #0
            blt triangleSkip
        @cross3
            ldr r9,[sp,#32]
            sub r2,r4,r9
            ldr r9,[sp,#36]
            sub r3,r8,r9

            ldr r0,[sp,#16]
            ldr r1,[sp,#20]

            mul r9,r2,r1
            mul r10,r3,r0
            sub r9,r9,r10

            cmp r9, #0
            blt triangleSkip


            mov r9,r4
            mov r10,r8
            bl TriangleKraso
        @rekina par punktu beigas
        triangleSkip:
        add r8,r8,#1
        b triangleLoop2Check
    triangleLoop2End:

    add r4,r4,#1
    b triangleLoop1Check
triangleLoop1End:
/***********************/

endTriangle:
add sp,sp,#24
ldmfd sp!,{r0-r10,lr}
bx lr

/***********************/

TriangleKraso:
@x=r9
@y=r10


sub sp,sp,#4
str lr,[sp]

@y ok
    cmp r10,#0
    blt endTriangleKraso

    bl FrameBufferGetHeight
    cmp r10,r0
    bge endTriangleKraso
@x ok
    cmp r9,#0
    blt endTriangleKraso

    bl FrameBufferGetWidth
    cmp r9,r0
    bge endTriangleKraso

@indekss
    mul r1,r10,r0
    add r1,r1,r9

bl FrameBufferGetAddress
ldr r2,CurColourAddr
ldr r2,[r2]

str r2,[r0,r1,LSL #2]

endTriangleKraso:
ldr lr,[sp]
add sp,sp,#4

bx lr












/*****************************************************************/

sum1:

add r0,r0,r1
mov r3,lr
mov r1,r0
bl myPrint
mov lr,r3
bx lr




myPrint:
stmfd sp!,{r0-r4,lr}
ldr r0, f_a
bl printf

ldmfd sp!,{r0-r4,lr}
bx lr



CurColourAddr: .word CurColour
            .comm CurColour,4,2


f_a: .word format
.data
format: .asciz "qw:%d\n"


