.text
.align 2
.global sum1
.type sum1, %function
.global asd1
.type asd1, %function

.global setPixColor
.type setPixColor, %function

.global getcurcol
.type getcurcol, %function

.global pixel
.type pixel, %function

.global circle
.type circle, %function



/******************************************************************/

setPixColor:
ldr r0,[r0]
ldr r1,CurColourAddr
str r0,[r1]
bx lr


getcurcol:
ldr r0,CurColourAddr
bx lr
/******************************************************************/

pixel:

ldr r2,[r2]
stmfd sp!,{r0-r5,lr}

@noskaidro vai 0<=x<=width
cmp r0,#0
blt pixelEnd


bl FrameBufferGetWidth
ldr r1,[sp]@x vertiba
cmp r1,r0
bge pixelEnd
@///////////////


@noskaidro vai 0<=y<=height
ldr r0,[sp,#4]
cmp r0,#0
blt pixelEnd


bl FrameBufferGetHeight
ldr r1,[sp,#4] @x vertiba

cmp r1,r0
bge pixelEnd
@///////////////


@iekraso pixeli(x,y) jeb F[y][x]
bl FrameBufferGetWidth
mov r4,r0
bl FrameBufferGetAddress
mov r5,r0


ldr r1,[sp]
ldr r2,[sp,#4]
ldr r3,[sp,#8]

mul r0,r2,r4
add r0,r0,r1


str r3,[r5,r0,LSL #2]
@///////////////


pixelEnd:
ldmfd sp!,{r0-r5,lr}
bx lr

/*****************************************************************/

circle:

stmfd sp!,{r0-r7,lr}
sub sp,sp,#12

bl FrameBufferGetWidth
str r0,[sp]
bl FrameBufferGetHeight
str r0,[sp,#4]
bl FrameBufferGetAddress
str r0,[sp,#8]



/****/
mov r2,#0
ldr r5,[sp,#20]
mov r6,#-1
mul r0,r5,r6

circleLoopCheck:
cmp r0,#0
bgt circleLoopEnd

circleLoop:
add r3,r2,#1
mul r5,r0,r0
mov r4,r5
mul r5,r3,r3
add r4,r4,r5
ldr r6,[sp,#20]
mul r5,r6,r6
sub r4,r4,r5

@sakas while
circleLoop2Check:
cmp r4,#0
bge circleLoop2End

circleLoop2:
add r2,r2,#1
add r3,r3,#1

mov r5,r2,LSL #1
add r5,r5,#1
add r4,r4,r5
b circleLoop2Check

circleLoop2End:
@beidzas while

mov r6,r4,LSL #1
sub r6,r6,r5

cmp r6,#0
bgt circleSkip1
add r2,r2,#1
circleSkip1:
mov r1,r2

@sakas krasosana
    @x,y
    ldr r6,[sp,#12]
    add r6,r6,r0
    ldr r7,[sp,#16]
    add r7,r7,r1
    @@
    mov r5,r1
    mov r1,r6
    bl myPrint
    mov r1,r7
    bl myPrint
    mov r1,r5
    @@

    bl circleKrasosana



    @x,-y
    ldr r7,[sp,#16]
    sub r7,r7,r1
    @@
    mov r5,r1
    mov r1,r6
    bl myPrint
    mov r1,r7
    bl myPrint
    mov r1,r5
    @@

    bl circleKrasosana

    @-x,-y
    ldr r6,[sp,#12]
    sub r6,r6,r0
    @@
    mov r5,r1
    mov r1,r6
    bl myPrint
    mov r1,r7
    bl myPrint
    mov r1,r5
    @@

    bl circleKrasosana

    @-x,y
    ldr r7,[sp,#16]
    add r7,r7,r1
    @@
    mov r5,r1
    mov r1,r6
    bl myPrint
    mov r1,r7
    bl myPrint
    mov r1,r5
    @@

    bl circleKrasosana


@beidzas krasosana

add r0,r0,#1
b circleLoopCheck
circleLoopEnd:
/****/


add sp,sp,#12
ldmfd sp!,{r0-r7,lr}
bx lr

/********/
circleKrasosana:

@vai y ok
cmp r7,#0
blt circleKrasosanaEnd
ldr r5,[sp,#4]
cmp r7,r5
bge circleKrasosanaEnd

@vai x ok
cmp r6,#0
blt circleKrasosanaEnd
ldr r5,[sp]
cmp r6,r5
bge circleKrasosanaEnd



@izrekina indeksu
mul r4,r7,r5
add r4,r4,r6

@krasa
ldr r5,CurColourAddr

@masiva norade
ldr r6,[sp,#8]

@nokraso
str r5,[r6,r4, LSL #2]


circleKrasosanaEnd:
bx lr





/*****************************************************************/
sum1:

add r0,r0,r1
mov r3,lr
mov r1,r0
bl myPrint
mov lr,r3
bx lr




myPrint:
stmfd sp!,{r0-r4,lr}
ldr r0, f_a
bl printf

ldmfd sp!,{r0-r4,lr}
bx lr



CurColourAddr: .word CurColour
            .comm CurColour,4,2


f_a: .word format
.data
format: .asciz "qw:%d\n"


