#include <stdio.h>
#include <stdlib.h>

#include "agra.h"

#define WIDTH 40
#define HEIGHT 20
pixcolor_t *Frame;

void FrameInit(){Frame=(pixcolor_t*)malloc(WIDTH*HEIGHT*4);}

int FrameBufferGetWidth(){return WIDTH;}
int FrameBufferGetHeight(){return HEIGHT;}
pixcolor_t* FrameBufferGetAddress(){return Frame;}

int FrameShow(){
    pixcolor_t cur;
    unsigned r,g,b;
    char c='?';
    int d1,d2,d3;
/*
    printf("frame show testa v\n\n");
    for(int i=0; i<WIDTH; ++i){
        for(int j=0; j<HEIGHT; ++j){
            if(Frame[i*WIDTH+j].r==0 && Frame[i*WIDTH+j].g==0 && Frame[i*WIDTH+j].b==0)printf(" ");
            else printf("*");
        }
        printf("\n");
    }
    printf("\n");
*/
    for(int y=0; y<HEIGHT; ++y){
        for(int x=0; x<WIDTH; ++x){
            cur=Frame[y*WIDTH+x];
            r=cur.r;
            g=cur.g;
            b=cur.b;
            r&=0b1111111111;
            g&=0b1111111111;
            b&=0b1111111111;
            d1=g+b;
            d2=r+b;
            d3=r+g;

            if(r>g&&r>b)c='R';
            else if(g>r&&g>b)c='G';
            else if(b>r&&b>g)c='B';
            else if(r==0&&g==0&&b==0)c=' ';
            else if(r==g&&g==b&&r!=0)c='*';
            else{
                //skatas dominances
                if(d1>d2&&d1>d3)c='C';
                else if(d2>d1&&d2>d3)c='M';
                else c='Y';
            }
            printf("%c",c);
        }
        printf("\n");
    }
    printf("\n");


    return 0;
}//FrameShow
