#ifndef AGRA_H_INCLUDED
#define AGRA_H_INCLUDED



typedef struct {
    unsigned int r  : 10;
    unsigned int g  : 10;
    unsigned int b  : 10;
    unsigned int op: 2;
} pixcolor_t;


typedef enum {
    PIXEL_COPY = 0,
    PIXEL_AND  = 1,
    PIXEL_OR   = 2,
    PIXEL_XOR  = 3
} pixop_t;


/****
///////////////////////////////////////////////////////////////////
*****/

int sum1(int a, int b);
int asd1();
pixcolor_t* getcurcol();
/****
///////////////////////////////////////////////////////////////////
*****/





/*

#ifndef WIDHT
#define WIDHT 10
#endif
#ifndef HEIGHT
#define HEIGHT 10
#endif
pixcolor_t* Frame;
*/







// Funkcija krasas (un operācijas) uztādīšanai
void setPixColor( pixcolor_t * colorop);

// Funkcija viena pikseļa uzstadīšanai
void pixel(int x, int y, pixcolor_t * colorop);

// Funkcija līnijas zīmēšanai starp punktiem
void line(int x1, int y1, int x2, int y2);

// Funkcija trijstūra aizpildīšanai ar tekošo krāsu
void triangleFill(int x1, int y1, int x2, int y2, int x3, int y3);

// Funkcija riņķa līnijas zīmēšanai
void circle(int x1, int y1, int radius);



// Kadra bufera sākuma adrese
pixcolor_t * FrameBufferGetAddress();

// Kadra platums
int FrameBufferGetWidth();

// Kadra augstums
int FrameBufferGetHeight();

// Kadra izvadīšana uz "displeja iekārtas".
int FrameShow();

#endif // AGRA_H_INCLUDED
